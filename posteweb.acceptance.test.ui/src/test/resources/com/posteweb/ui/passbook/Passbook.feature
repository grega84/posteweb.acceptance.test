@tag
Feature: Passbook

 # @tag1

 # Scenario: money transfer from account to passbook
  #  Given the user go to page "https://www.poste.it/"
  #  And log-in in personal area with followings credential
  #    | user                   | password  |
   #   | salvatore.musella-1959 | Musabp01! |
  #  And navigate to "Conto Bancoposta" page
   # When compile a "Girofondo da conto a libretto Smart" form with followings data
   #   | account | bookletNumber | amount | description |
   #   |         |               |        |             |
   # And confirm the posteID from Bancoposta App
   # Then the operation successfull completed
      #| type           | number     |
     # | Libretto Smart | 1243454566 |
   # And in the  "Libretto smart" page the last movement are the following
    #  | type           | number     |
    #  | Libretto Smart | 1243454566 |

  Scenario: Verificare i risultati della ricerca dal campo Cerca movimenti della card di un libretto
     Given the user go to page "https://www.poste.it/"
    	And log-in in personal area with followings credential
      | user                   | password  |
      | vincenzo.fortunato-4869 | D@niela22Lili10 |
      When l'utente si sposta nella sezione consulta libretto
      And accede al tab Libretto
      |tipoLibretto|codice|
      |Ordinario|000049573986|
      And accede al tab Movimenti
      When l'utente inserisce la chiave di ricerca
      |searchKey|
      |GIROFONDO|
      And clicca su esegui ricerca ed imposta la vista a "40" item
      Then la lista dei movimenti prensenti "1" elementi di quelli riportati nella seguente lista
      |data|descrizione|importo|
			#|02 apr 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,01|
			#|27 mar 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,01|
			#|07 mar 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,01|
			#|04 mar 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,10|
			#|28 feb 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,10|
			|28 feb 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,10|
			#|28 feb 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,10|
			#|26 feb 2019|	GIROFONDO DA WEB DA LIBRETTO DR 000049580610|+0,10|
			#|07 feb 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,10|
			#|25 gen 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,20|
			#|18 gen 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,20|
			#|18 gen 2019|	GIROFONDO DA APP DA LIBRETTO DR 000049580610|+0,20|
      
Scenario: money transfer from account to passbook
  Given the user go to page "https://www.poste.it/"
    	And log-in in personal area with followings credential
      |fullName| user                   | password  | posteId|
      |ANGELO CASORIA| Angelo.casoria-xell | Ang_cas83 | 555555|
    	And the user navigate in section consulta libretto
      And select the tab related to the folowing data
      |tipoLibretto|codice|
      |Smart|000049492562|
      And get the saldo Amount
      And navigate to "MyPage" page
    	And navigate to "Conto Bancoposta" page
    	And navigate to "SaldoEMovimentiPage" page 
    	And get the saldo Amount
      When compile a "Girofondo da conto a libretto Smart" form with followings data
  		|numberConto|bookletNumber|amount|description|
  		|Conto BancoPosta n. 000084627371	|000049492562	|2,00	| Test Automation	|
  		And confirm the posteID from Bancoposta App 
   		Then the operation successfull completed 
 			 And navigate to "MyPage" page
    	And navigate to "Conto Bancoposta" page
    	And navigate to "SaldoEMovimentiPage" page 
    	And check that the Saldo is correctly decreased or improved of "-2,00"
    	 And navigate to "MyPage" page
      And the user navigate in section consulta libretto
      And select the tab related to the folowing data
      |tipoLibretto|codice|
      |Smart|000049492562|
      And check that the Saldois correctly decreased or improved of "+2,00"
      
      