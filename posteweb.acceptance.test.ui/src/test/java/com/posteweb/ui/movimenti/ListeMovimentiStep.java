package com.posteweb.ui.movimenti;

import java.util.List;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import datatablebean.CheckDatailsSummaryDatatable;
import datatablebean.UserCredentialDatatable;
import posteweb.automation.core.UI.BancoPostaInternetBankingPage;
import posteweb.automation.core.UI.CruscottoPage;
import posteweb.automation.core.UI.HomePage;
import posteweb.automation.core.UI.SaldoEMovimentiPage;
import test.automation.core.UIUtils;

public class ListeMovimentiStep {
	private WebDriver driver;
	private HomePage homePage;
	private CruscottoPage cruscottoPage;
	private BancoPostaInternetBankingPage bancoPostaInternetBankingPage;
	private SaldoEMovimentiPage saldoEMovimentiPage;
	
	@Given("^la pagina del portale di poste viene correttamente caricata$")
	public void la_pagina_del_portale_di_poste_viene_correttamente_caricata() throws Throwable {
		driver=UIUtils.ui().driver();
		driver.manage().window().fullscreen();
    	homePage = (HomePage) new HomePage(driver).get();
    	homePage.closeAlert();
		UIUtils.ui().takeScreenshot(driver);
	}


    @And("^esegue l'accesso con le seguenti utenze$")
    public void esegue_laccesso_con_le_seguenti_utenze(List<UserCredentialDatatable> credenziali) throws Throwable {
    	cruscottoPage=homePage.eseguiLogin(credenziali.get(0));
    	UIUtils.ui().takeScreenshot(driver);
    }
 
    @And("^dalla pagina di cruscotto personale accedere al dettaglio\"([^\"]*)\"$")
    public void dalla_pagina_di_cruscotto_personale_accedere_al_dettaglio(String biscottoName) throws Throwable {
    	bancoPostaInternetBankingPage=(BancoPostaInternetBankingPage) cruscottoPage.goToBiscottoPage(biscottoName);
    	UIUtils.ui().takeScreenshot(driver);
    }
    @When("^l'utente accede alla funzionalita \"([^\"]*)\"$")
    public void l_utente_accede_alla_funzionalita(String functionName) throws Throwable {
    	saldoEMovimentiPage=(SaldoEMovimentiPage)bancoPostaInternetBankingPage.navigateToFunction(functionName);
    	UIUtils.ui().takeScreenshot(driver);
    }

    @Then("^nella pagina delle movimentazioni appaiono i seguenti dati$")
    public void nella_pagina_delle_movimentazioni_appaiono_i_seguenti_dati(List<CheckDatailsSummaryDatatable> fieldsToCheck) throws Throwable {
    	saldoEMovimentiPage.checkDetailsInBoxAdviceSummary(fieldsToCheck);
    	UIUtils.ui().takeScreenshot(driver);
    }
    
    @After
    private void tearDown(){
    	driver.quit();
    };
}
