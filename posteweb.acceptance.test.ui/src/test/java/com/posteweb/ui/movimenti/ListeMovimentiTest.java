package com.posteweb.ui.movimenti;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/ListeMovimenti.json" },
features = {"src/test/resources/com/posteweb/ui/movimenti/ListaMovimenti.feature"})
@Ui
public class ListeMovimentiTest {

}
