package com.posteweb.ui.passbook;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import test.automation.core.XCucumber;
import test.automation.core.annotation.Ui;

@RunWith(XCucumber.class)
@CucumberOptions(plugin = { "json:target/reports/Passbook.json" },
features = {"src/test/resources/com/posteweb/ui/passbook/Passbook.feature"})
@Ui
public class PassbookTest {

}
