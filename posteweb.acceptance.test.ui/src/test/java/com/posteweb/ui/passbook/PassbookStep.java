package com.posteweb.ui.passbook;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import datatablebean.BookletMovementSearchFilter;
import datatablebean.CheckDatailsSummaryDatatable;
import datatablebean.GiroFondoBean;
import datatablebean.LibrettoBean;
import datatablebean.MovimentiBean;
import datatablebean.SearchMovimentiBean;
import datatablebean.UserCredentialDatatable;
import posteweb.automation.core.UI.BancoPostaInternetBankingPage;
import posteweb.automation.core.UI.CruscottoPage;
import posteweb.automation.core.UI.HomePage;
import posteweb.automation.core.UI.HomePostalSavingsPage;
import posteweb.automation.core.UI.PageUtility;
import posteweb.automation.core.UI.PassBookPage;
import posteweb.automation.core.UI.SaldoEMovimentiPage;
import posteweb.automation.core.UI.SendMoneyFromContoPage;
import test.automation.core.UIUtils;

public class PassbookStep {
	private WebDriver driver;
	private HomePage homePage;
	private HomePostalSavingsPage postalSaving;
	private CruscottoPage cruscottoPage;
	private BancoPostaInternetBankingPage bancoPostaInternetBankingPage;
	private SaldoEMovimentiPage saldoEMovimentiPage;
	private PassBookPage passPage;
	private UserCredentialDatatable credentialBean;

	@Given("^log-in in personal area with followings credential$")
	public void log_in_in_personal_area_with_followings_credential(List<UserCredentialDatatable> credenzialAccess) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		credentialBean=credenzialAccess.get(0);
		cruscottoPage=homePage.eseguiLogin(credenzialAccess.get(0));
		//    	UIUtils.ui().takeScreenshot(driver);
	}


	private SendMoneyFromContoPage sendMoneyFromContoPage;

	@When("^compile a \"([^\"]*)\" form with followings data$")
	public void compile_a_form_with_followings_data(String tab, List<GiroFondoBean> girofondoBean) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		bancoPostaInternetBankingPage.compileGirofondo(girofondoBean.get(0),this.credentialBean);

	}

	@When("^confirm the posteID from Bancoposta App$")
	public void confirm_the_posteID_from_Bancoposta_App() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//        throw new PendingException();
	}

	@Then("^the operation successfull completed$")
	public void the_operation_successfull_completed() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//        throw new PendingException();
		WebElement closePopUp= UIUtils.ui().waitForCondition(driver, ExpectedConditions.visibilityOfElementLocated(By.xpath("//img[contains(@src,'annulla.png')]")),40);
		closePopUp.click();
	}

	@Given("^the user go to page \"([^\"]*)\"$")
	public void the_user_go_to_page(String sitePage) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		driver=UIUtils.ui().driver();
		driver.manage().window().fullscreen();
		homePage = (HomePage) new HomePage(driver).get();
		homePage.closeAlert();

	}

	@When("^navigate to \"([^\"]*)\" page$")
	public void navigate_to_page(String page) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		if(page.equals("Send Money")){
			bancoPostaInternetBankingPage=(BancoPostaInternetBankingPage) cruscottoPage.goToBiscottoPage("Conto Bancoposta");

		}
		else if(page.equals("Conto Bancoposta")){
			bancoPostaInternetBankingPage=(BancoPostaInternetBankingPage) cruscottoPage.goToBiscottoPage("Conto Bancoposta");

		}else if(page.equals("MyPage")){
			PageUtility pUtility= new PageUtility();
			Thread.sleep(3000);
			cruscottoPage=pUtility.goToCruscottoPage(driver,this.credentialBean.getFullName());
		}else if(page.equals("SaldoEMovimentiPage")){
			bancoPostaInternetBankingPage.navigateToFunction("Saldo e movimenti");
			Thread.sleep(5000);
		}

	}
	@Then("^close the browser$")
	public void close_the_browser() throws Throwable {
		driver.quit();
	
	
	
	}
	@Then("^in the  \"([^\"]*)\" page the last movement are the following$")
	public void in_the_page_the_last_movement_are_the_following(String arg1) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		//        throw new PendingException();
	}

	@When("^l'utente si sposta nella sezione consulta libretto$")
	public void l_utente_si_sposta_nella_sezione_consulta_libretto() throws Throwable 
	{
		cruscottoPage.gotoConsultaLibrettoAndClick();
	}

	@When("^accede al tab Libretto$")
	public void accede_al_tab_Libretto(List<LibrettoBean> L) throws Throwable 
	{
		this.postalSaving=new HomePostalSavingsPage(driver);
		this.passPage=this.postalSaving.gotoPassBookByLink(L.get(0).getTipoLibretto(),L.get(0).getCodice());
	}

	@When("^accede al tab Movimenti$")
	public void accede_al_tab_Movimenti() throws Throwable 
	{
		this.passPage.clickHistoryLink();
	}

	@When("^l'utente inserisce la chiave di ricerca$")
	public void l_utente_inserisce_la_chiave_di_ricerca_searchKey(List<SearchMovimentiBean> a) throws Throwable 
	{
		this.passPage.SearchHistory(a.get(0).getSearchKey());
	}

	@When("^clicca su esegui ricerca ed imposta la vista a \"([^\"]*)\" item$")
	public void clicca_su_esegui_ricerca_ed_imposta_la_vista_a_item(String pagNum) throws Throwable 
	{
		this.passPage.clickSearchButton(pagNum);

	}


	@Then("^la lista dei movimenti prensenti \"([^\"]*)\" elementi di quelli riportati nella seguente lista$")
	public void la_lista_dei_movimenti_presenta_la_chiave_ricercata(String s,List<MovimentiBean> A) throws Throwable 
	{
		//this.passPage.CheckSearchKey(a.get(0).getSearchKey());
		this.passPage.CheckSearchKey(A,s.equals("tutti") ? "complete" : s);
	}

	//----------------------

	@Given("^the user navigate in section consulta libretto$")
	public void the_user_navigate_in_section_consulta_libretto() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		cruscottoPage.gotoConsultaLibrettoAndClick();
//		Thread.sleep(5000);

	}

	@Given("^select the tab related to the folowing data$")
	public void select_the_tab_related_to_the_folowing_data(List<LibrettoBean> bookletData) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		this.postalSaving=new HomePostalSavingsPage(driver);
		this.passPage=this.postalSaving.gotoPassBookByLink(bookletData.get(0).getTipoLibretto(),bookletData.get(0).getCodice());
		Thread.sleep(10000);
	}

	@Given("^search the movement with filter$")
	public void search_the_movement_with_filter(List<BookletMovementSearchFilter> filter) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		passPage.clickHistoryLink();
		passPage.searchWithFilter(filter.get(0));
	}

	@Given("^check the movement list is empty$")
	public void check_the_movement_list_is_empty() throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		passPage.checkEmptyListAfterSearch();
	}

	@Then("^check that in Libretto page for the following filters$")
	public void check_that_in_Libretto_page_for_the_following_filters(List<BookletMovementSearchFilter> filter) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		passPage.searchWithFilter(filter.get(0));
	}

	@Then("^the movement List show \"([^\"]*)\" element with this values$")
	public void the_movement_List_show_element_with_this_values(String s,List<MovimentiBean> A) throws Throwable 
	{
		//this.passPage.CheckSearchKey(a.get(0).getSearchKey());
		this.passPage.CheckSearchKey(A,s.equals("tutti") ? "complete" : s);
	}
	@Given("^get the saldo Amount$")
	public void get_the_saldo_Amount() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(5000);
		
		//	    throw new PendingException();
	}

	@Then("^check that the Saldo is correctly decreased or improved of \"([^\"]*)\"$")
	public void check_that_the_Saldo_is_correctly_decreased_or_improved_of(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
		Thread.sleep(4000);
	}

	@Then("^check that the Saldois correctly decreased or improved of \"([^\"]*)\"$")
	public void check_that_the_Saldois_correctly_decreased_or_improved_of(String arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
//	    throw new PendingException();
		Thread.sleep(4000);
	}




//	@After
//	private void tearDown() throws Exception{
//		Thread.sleep(13000);
////		driver.quit();
//	};
}
